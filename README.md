# crossboard-front
Simple community website frontend (Vue/Javascript)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Deploy settings
```
# Install yarn
curl -o- -L https://yarnpkg.com/install.sh | bash

# Add environment variables to profile
vi ~/.bash_profile
# Add new line: 
>> export PATH=“$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH”

source ~/.bash_profile

# Install aws client
pip install awscli

# Deploy to S3 server
npm run deploy 
# for real, npm run deploy_real
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
