import Vue from 'vue';
import store from '@/store';

Vue.mixin({
  methods: {
    phoneFormat(evt) {
      const keysAllowed = '0123456789- '.split('');
      const keyPressed = evt.key;
      if (!keysAllowed.includes(keyPressed)) {
        evt.preventDefault();
      } else {
        return true;
      }
      return false;
    },
    numberFormat(evt) {
      const keysAllowed = '0123456789'.split('');
      const keyPressed = evt.key;
      if (!keysAllowed.includes(keyPressed)) {
        evt.preventDefault();
      } else {
        return true;
      }
      return false;
    },
    onClickLogout() {
      store.dispatch('LOGOUT').then(() => {
        if (this.$route.name !== 'Home') this.$router.push('/');
      });
    },
    onClickLanguage(lang) {
      store.dispatch('SET_LANG', lang);
      store.dispatch('GET_USER', 'me');
    },
  },
});
