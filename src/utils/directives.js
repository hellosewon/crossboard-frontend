import Vue from 'vue';

Vue.directive('dummyDirective', {
  bind(el) {
    console.log(el);
  },
});
