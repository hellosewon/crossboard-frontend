import Vue from 'vue';

Vue.filter('striphtml', (value) => {
  const div = document.createElement('div');
  div.innerHTML = value;
  const text = div.textContent || div.innerText || '';
  return text;
});

Vue.filter('truncate', (text, stop, clamp) => text.slice(0, stop) + (stop < text.length ? clamp || '...' : ''));

Vue.filter('laststr', (text, num) => text.toString().slice(-num));

Vue.filter('cntlimited', (count, limitDigit = 10) => {
  const cnt = Number(count);
  if (cnt === 0) {
    return '-';
  }
  if (cnt < 10 ** limitDigit) {
    return cnt.toLocaleString().toString();
  }
  return `${(10 ** limitDigit - 1).toLocaleString().toString()}+`;
});
