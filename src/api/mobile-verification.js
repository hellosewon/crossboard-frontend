import axios from 'axios';

class MobileVerifier {
  constructor(purpose = null, status = null, phoneCode = null, phoneNumber = null) {
    this.purpose = purpose ?? ['REGISTRATION', 'PASSWORD_RESET', 'PHONE_CHANGE'][0];
    this.status = status ?? ['INITIAL', 'REQUESTED', 'VERIFIED', 'FAILED'][0];
    this.phoneCode = phoneCode ?? '';
    this.phoneNumber = phoneNumber ?? '';
    this.timer = null;
    this.timerCounter = null;
    this.verificationCode = null;
  }

  getVerificationCode(phoneCode, phoneNumber) {
    // TODO: Validation
    axios.get(`${process.env.VUE_APP_ENDPOINT}/auth/mobile-verification`, {
      params: {
        phone_code: phoneCode,
        phone_number: phoneNumber,
        purpose: this.purpose,
      },
    }).then((res) => {
      if (res.status === 202) {
        this.status = 'REQUESTED';
        this.timer = res.data.expire_time;
        if (this.timerCounter === null) {
          this.timerCounter = setInterval(() => {
            this.timer -= 1;
            if (this.timer === 0) {
              clearInterval(this.timerCounter);
            }
          }, 1000);
        }
      }
    }).catch((error) => {
      switch (error.response.status) {
        case 404:
          alert('계정이 존재하지 않습니다.');
          break;
        case 409:
          alert('이미 존재하는 회원입니다.');
          break;
        default:
          alert('알 수 없는 에러');
          // eslint-disable-next-line
          console.error(error);
          break;
      }
    });
  }

  postVerificationCode(phoneCode, phoneNumber) {
    // TODO: Validation
    axios.post(`${process.env.VUE_APP_ENDPOINT}/auth/mobile-verification`, {
      phone_code: phoneCode,
      phone_number: phoneNumber,
      purpose: this.purpose,
      verification_code: this.verificationCode,
    }).then((res) => {
      if (res.status === 200) {
        this.status = 'VERIFIED';
        clearInterval(this.timerCounter);
      }
    }).catch((error) => {
      switch (error.response.status) {
        default:
          this.status = 'FAILED';
          alert('알 수 없는 에러');
          // eslint-disable-next-line
          console.error(error);
          break;
      }
    });
  }
}

export default MobileVerifier;
