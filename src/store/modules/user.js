import axios from 'axios';

const user = {
  state: {
    userInfo: null,
  },
  getters: {
    userInfo(state) {
      return state.userInfo;
    },
    isSuperuser(state) {
      return state.userInfo.is_superuser;
    },
    userInfoCountPosts(state) {
      if (state.userInfo === null) {
        return null;
      }
      return state.userInfo.count_posts;
    },
    userInfoCountComments(state) {
      if (state.userInfo === null) {
        return null;
      }
      return state.userInfo.count_comments;
    },
  },
  mutations: {
    GET_USER(state, userInfo) {
      state.userInfo = userInfo;
    },
  },
  actions: {
    GET_USER({ commit }, publicId) {
      return axios.get(`${process.env.VUE_APP_ENDPOINT}/users/${publicId}`, {})
        .then((res) => {
          commit('GET_USER', res.data);
        });
    },
  },
};

export default user;
