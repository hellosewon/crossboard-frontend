import axios from 'axios';
import i18n from '@/translations';

const auth = {
  state: {
    accessToken: null,
    userInfo: null,
    lang: null,
  },
  getters: {
    isAuthenticated(state) {
      return Boolean(state.accessToken);
    },
    userInfo(state) {
      return state.userInfo;
    },
    lang(state) {
      return state.lang || 'en';
    },
  },
  mutations: {
    REGISTER(state, data) {
      const { accessToken, userInfo } = data;
      state.accessToken = accessToken;
      state.userInfo = userInfo;
      state.lang = userInfo.lang;
      localStorage.accessToken = accessToken;
      localStorage.userInfo = JSON.stringify(userInfo);
      axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
      axios.defaults.headers.common.Language = `${state.userInfo.lang}`;
      i18n.locale = state.lang;
    },
    LOGIN(state, data) {
      const { accessToken, userInfo } = data;
      state.accessToken = accessToken;
      state.userInfo = userInfo;
      state.lang = userInfo.lang;
      localStorage.accessToken = accessToken;
      localStorage.userInfo = JSON.stringify(userInfo);
      axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
      axios.defaults.headers.common.Language = `${state.userInfo.lang}`;
      i18n.locale = state.lang;
    },
    AUTO_LOGIN(state) {
      // 토큰 정보 가져오기
      const { accessToken, userInfo, lang } = localStorage;
      if (!accessToken || accessToken === null || !userInfo || userInfo === null) {
        state.accessToken = null;
        state.userInfo = null;
        delete localStorage.accessToken;
        delete localStorage.userInfo;
        delete axios.defaults.headers.common.Authorization;
        if (!lang || lang === null) {
          state.lang = null;
          delete localStorage.lang;
          delete axios.defaults.headers.common.Language;
        } else {
          state.lang = lang;
          axios.defaults.headers.common.Language = `${lang}`;
          i18n.locale = state.lang;
        }
      } else {
        state.accessToken = accessToken;
        state.userInfo = JSON.parse(userInfo);
        state.lang = state.userInfo.lang;
        axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
        axios.defaults.headers.common.Language = `${state.userInfo.lang}`;
        i18n.locale = state.lang;
      }
    },
    LOGOUT(state) {
      state.accessToken = null;
      state.userInfo = null;
      delete localStorage.accessToken;
      delete localStorage.userInfo;
      delete axios.defaults.headers.common.Authorization;
      if (!localStorage.lang || localStorage.lang === null) {
        state.lang = null;
        delete localStorage.lang;
        delete axios.defaults.headers.common.Language;
        i18n.locale = state.lang;
      } else {
        state.lang = localStorage.lang;
        axios.defaults.headers.common.Language = `${localStorage.lang}`;
        i18n.locale = state.lang;
      }
    },
    LOCAL_LOGOUT(state) {
      state.accessToken = null;
      state.userInfo = null;
      delete localStorage.accessToken;
      delete localStorage.userInfo;
      delete axios.defaults.headers.common.Authorization;
      if (!localStorage.lang || localStorage.lang === null) {
        state.lang = null;
        delete localStorage.lang;
        delete axios.defaults.headers.common.Language;
        i18n.locale = state.lang;
      } else {
        state.lang = localStorage.lang;
        axios.defaults.headers.common.Language = `${localStorage.lang}`;
        i18n.locale = state.lang;
      }
    },
    SET_LANG(state, lang) {
      state.lang = lang;
      if (this.getters.isAuthenticated) {
        state.userInfo.lang = lang;
        localStorage.userInfo = JSON.stringify(state.userInfo);
      } else {
        localStorage.lang = lang;
      }
      axios.defaults.headers.common.Language = `${lang}`;
      i18n.locale = lang;
    },
  },
  actions: {
    REGISTER({ commit }, userModel) {
      return axios.post(`${process.env.VUE_APP_ENDPOINT}/users`, userModel)
        .then((res) => {
          commit('REGISTER', res.data);
        });
    },
    LOGIN({ commit }, loginModel) {
      return axios.post(`${process.env.VUE_APP_ENDPOINT}/auth/login`, loginModel)
        .then((res) => {
          commit('LOGIN', res.data);
        });
    },
    AUTO_LOGIN({ commit }) {
      commit('AUTO_LOGIN');
    },
    LOGOUT({ commit }) {
      return axios.post(`${process.env.VUE_APP_ENDPOINT}/auth/logout`, {})
        .then((res) => {
          commit('LOGOUT', res.data);
        }).catch(() => {
          commit('LOCAL_LOGOUT');
        });
    },
    LOCAL_LOGOUT({ commit }) {
      commit('LOCAL_LOGOUT');
    },
    SET_LANG({ commit }, lang) {
      commit('SET_LANG', lang);
    },
  },
};

export default auth;
