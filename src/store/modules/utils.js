import axios from 'axios';

const user = {
  state: {
    ipAddress: null,
  },
  getters: {
    ipAddress(state) {
      return state.ipAddress;
    },
    isOfficeIP(state) {
      if (!process.env.VUE_APP_ADMIN_IP_LIST) return null;
      return process.env.VUE_APP_ADMIN_IP_LIST.split(',').includes(state.ipAddress);
    },
  },
  mutations: {
    GET_IP(state, ipAddress) {
      console.log(ipAddress);
      state.ipAddress = ipAddress;
    },
  },
  actions: {
    GET_IP({ commit }) {
      return axios.get(`${process.env.VUE_APP_ENDPOINT}/auth/ip-address`, {})
        .then((res) => {
          commit('GET_IP', res.data.ip_address);
        });
    },
  },
};

export default user;
