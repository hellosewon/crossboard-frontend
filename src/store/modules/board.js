import axios from 'axios';

const board = {
  state: {
    boardList: [],
    browsingBoardGroup: null,
    browsingBoardList: [],
    browsingBoard: null,
  },
  getters: {
    boardList(state) {
      return state.boardList;
    },
    browsingBoardGroup(state) {
      return state.browsingBoardGroup;
    },
    browsingBoardList(state) {
      return state.browsingBoardList;
    },
    browsingBoard(state) {
      return state.browsingBoard;
    },
  },
  mutations: {
    GET_BOARD_LIST(state, data) {
      state.boardList = data.data;
    },
    SET_BOARD_GROUP(state, boardGroup) {
      state.browsingBoardGroup = boardGroup;
      localStorage.browsingBoardGroup = boardGroup;
      state.browsingBoardList = state.boardList.filter((obj) => obj.board_group === boardGroup);
    },
    AUTO_SET_BOARD_GROUP(state) {
      // 토큰 정보 가져오기
      if (this.getters.isAuthenticated && this.getters.userInfo.preferred_country) {
        state.browsingBoardGroup = this.getters.userInfo.preferred_country;
        state.browsingBoardList = state.boardList.filter(
          (obj) => obj.board_group === this.getters.userInfo.preferred_country,
        );
      } else {
        const { browsingBoardGroup } = localStorage;
        if (!browsingBoardGroup || browsingBoardGroup === null) {
          state.browsingBoardGroup = null;
          delete localStorage.browsingBoardGroup;
        } else {
          state.browsingBoardGroup = browsingBoardGroup;
          state.browsingBoardList = state.boardList.filter(
            (obj) => obj.board_group === browsingBoardGroup,
          );
        }
      }
    },
    SET_BOARD(state, urlNamespace) {
      const selectedBoard = state.boardList
        .filter((obj) => obj.url_namespace === urlNamespace).pop();
      if (selectedBoard) {
        state.browsingBoardGroup = selectedBoard.board_group;
        state.browsingBoardList = state.boardList.filter(
          (obj) => obj.board_group === selectedBoard.board_group,
        );
        state.browsingBoard = selectedBoard;
      } else {
        // TODO: 입력한 urlNamespace의 게시판 없는 경우 화면 처리
      }
    },
  },
  actions: {
    GET_BOARD_LIST({ commit }, browsingBoardGroup) {
      return axios.get(`${process.env.VUE_APP_ENDPOINT}/boards`, {
        params: {
          board_group: browsingBoardGroup,
        },
      }).then((res) => {
        commit('GET_BOARD_LIST', res.data);
      }).catch((error) => {
        switch (error.response.status) {
          default:
            alert('알 수 없는 에러');
            // eslint-disable-next-line
            console.error(error);
            break;
        }
      });
    },
    async SET_BOARD_GROUP({ dispatch, commit }, boardGroup) {
      await dispatch('GET_BOARD_LIST');
      commit('SET_BOARD_GROUP', boardGroup);
    },
    async AUTO_SET_BOARD_GROUP({ dispatch, commit }) {
      await dispatch('AUTO_LOGIN');
      await dispatch('GET_BOARD_LIST');
      commit('AUTO_SET_BOARD_GROUP');
    },
    async SET_BOARD({ dispatch, commit }, urlNamespace) {
      await dispatch('GET_BOARD_LIST');
      commit('SET_BOARD', urlNamespace);
    },
  },
};

export default board;
