import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import board from './modules/board';
import user from './modules/user';
import utils from './modules/utils';

Vue.use(Vuex);
// const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  modules: {
    auth,
    board,
    user,
    utils,
  },
  strict: false,
});
