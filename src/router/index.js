import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import Home from '@/views/Home.vue';
import About from '@/views/about/About.vue';
import Agreement from '@/views/about/Agreement.vue';
import Register from '@/views/auth/Register.vue';
import Login from '@/views/auth/Login.vue';
import PasswordReset from '@/views/auth/PasswordReset.vue';
import MyPage from '@/views/MyPage.vue';
import Admin from '@/views/Admin.vue';
import BoardView from '@/views/board/BoardView.vue';
import PostView from '@/views/board/PostView.vue';
import PostWrite from '@/views/board/PostWrite.vue';
import MobileMenu from '@/views/MobileMenu.vue';

Vue.use(VueRouter);
// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';

// const NotFound = { template: '<div>Not Found</div>' };

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/about/terms',
    name: 'Terms',
    component: Agreement,
    props: { document: 'terms' },
  },
  {
    path: '/about/privacy',
    name: 'Privacy',
    component: Agreement,
    props: { document: 'privacy' },
  },
  {
    path: '/about/sms-receive',
    name: 'SMSReceive',
    component: Agreement,
    props: { document: 'sms-receive' },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { requiresAnon: true },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { requiresAnon: true },
  },
  {
    path: '/password-reset',
    name: 'PasswordReset',
    component: PasswordReset,
  },
  {
    path: '/my-page',
    name: 'MyPage',
    component: MyPage,
    meta: { requiresLogin: true },
  },
  {
    path: '/admin',
    name: 'Admin',
    redirect: '/admin/users',
    meta: { requiresSuperuser: true },
  },
  {
    path: '/admin/users',
    name: 'AdminUsers',
    component: Admin,
    props: { menu: 'users' },
    meta: { requiresSuperuser: true },
  },
  {
    path: '/admin/secedes',
    name: 'AdminSecedes',
    component: Admin,
    props: { menu: 'secedes' },
    meta: { requiresSuperuser: true },
  },
  {
    path: '/admin/superusers',
    name: 'AdminSuperusers',
    component: Admin,
    props: { menu: 'superusers' },
    meta: { requiresSuperuser: true },
  },
  {
    path: '/boards/:urlNamespace',
    name: 'BoardView',
    component: BoardView,
  },
  {
    path: '/boards/:urlNamespace/posts/:postId',
    name: 'PostView',
    component: PostView,
  },
  {
    path: '/boards/:urlNamespace/write',
    name: 'PostWrite',
    component: PostWrite,
    props: { action: 'write' },
  },
  {
    path: '/boards/:urlNamespace/posts/:postId/modify',
    name: 'PostModify',
    component: PostWrite,
    props: { action: 'modify' },
  },

  // Mobile Menu
  {
    path: '/mobile-menu',
    name: 'MobileMenu',
    component: MobileMenu,
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if (store.accessToken === null || store.accessToken === undefined) {
    store.dispatch('AUTO_LOGIN');
  }
  if (to.matched.some((record) => record.meta.requiresLogin)
      && !store.getters.isAuthenticated) {
    next('/login');
  } else if (to.matched.some((record) => record.meta.requiresAnon)
      && store.getters.isAuthenticated) {
    next('/');
  } else if (to.matched.some((record) => record.meta.requiresSuperuser)) {
    if (!store.getters.isAuthenticated) next('/');
    if (store.getters.ipAddress === null || store.getters.ipAddress === undefined) {
      store.dispatch('GET_IP').then(() => {
        if (!store.getters.isOfficeIP) next('/');
        if (!store.getters.userInfo.is_superuser) next('/');
        next();
      });
    } else {
      if (!store.getters.isOfficeIP) next('/');
      if (!store.getters.userInfo.is_superuser) next('/');
      next();
    }
  } else {
    next();
  }
});

export default router;
