import Vue from 'vue';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './translations';
import './mixins';
import '@/utils/filters';
import '@/utils/directives';

// Axios Global Response Interceptor
/*
axios.interceptors.response.use((response) => response, (error) => {
  console.log(error.response.data.message);
  switch (error.response.status) {
    case 401:
      if (['Token blacklisted', 'Signature expired'].includes(error.response.data.message)) {
        store.dispatch('LOCAL_LOGOUT');
        router.go();
        return;
      }
      throw error;
    default:
      console.log('asd');
      throw error;
  }
});
*/

Vue.config.productionTip = false;

// Moment
momentDurationFormatSetup(moment);
Vue.prototype.moment = moment;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
  methods: {
  },
  created() {
    this.$store.dispatch('GET_IP');
    this.$store.dispatch('AUTO_LOGIN');
    this.$store.dispatch('GET_BOARD_LIST', null);
    this.$store.dispatch('AUTO_SET_BOARD_GROUP');
  },
}).$mount('#app');
